UNAME=$(shell uname)

target = qualita
deps = Makefile *.tex

all: $(target).pdf

$(target).pdf: $(target).tex $(deps)
	pdflatex $(target).tex
	pdflatex $(target).tex

%.1: %.mp
	texexec --mptex $<

.PHONY: clean view backup

clean:
	rm -f $(target).{aux,dvi,log,out,toc,lof,pdf} *.{1,mpx,mpo,log} tmpgraph* *.tmp mpgraph.mp *~

view: $(target).pdf
ifeq ($(UNAME),Darwin)
	open -a /Applications/Preview.app $(target).pdf
else
	xpdf $(target).pdf $(PAGE)
endif

backup:
	tar zcvf ../$(target)-backup.tar.gz .
